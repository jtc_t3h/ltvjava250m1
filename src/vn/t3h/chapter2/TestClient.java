package vn.t3h.chapter2;

import java.util.StringTokenizer;

public class TestClient {

	private static final float PI = 3.14F;
	
	public static void main(String[] args) {

		// khác biệt giữa kiểu cơ bản và tham chiếu		
		int i = 10;
		String s = "hello";
		
		// chuyển đổi kiểu dữ liệu
		byte b = 100;
		long l = 20;
		
		b = (byte)l;
		l = b;
		
		// xu ly ki tu
		char a = 'a';
		char cB = 66; // mã accii tương ứng với ký tự
		System.out.println(cB);
		
		Character c = new Character('a');
		
		// So sánh chuỗi
		String s1 = "hello";
		String s2 = new String("hello");
		String s3 = "hello";
		
		System.out.println(s1 == s2); //  false
		System.out.println(s1.equals(s2)); // true
		System.out.println(s1 == s3); // true => s1 và s3 cùng tham chiếu tới 1 ô nhớ
		
		// immutable
		System.out.println(s.replaceAll("l", "h"));
		System.out.println(s); // hello  | hehho
		
		for (int idx = 0; idx < 100; idx++) {
			s += "hello";
		}
		
		String s4 = "abc";
		String s5 = "add";
		System.out.println(s4.compareTo(s5));
		
		StringBuilder sb = new StringBuilder();
		sb.append("Hello ");
		sb.append("world");
		
		String csvText = "0901564797, MOBICARD, CN";
		StringTokenizer st = new StringTokenizer(csvText, ",");
		System.out.println(st.countTokens()); // 3
		while (st.hasMoreTokens()) {
			System.out.println(st.nextToken());
		}
		System.out.println(st.countTokens());
	}

	public static void aFunction() {
		// không gọi được biến khai báo trong hàm main()
		
	}
}
