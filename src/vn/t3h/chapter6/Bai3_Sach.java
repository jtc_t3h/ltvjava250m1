package vn.t3h.chapter6;

import java.util.Date;

public class Bai3_Sach {

	// thuộc tính -> Thông tin chung
	private String maSach;
	private String tenSach;
	private Date ngayNhap;
	private String nxb;
	private int donGia;
	private int soLuong;
	
	// Phuong thuc khoi tao
	public Bai3_Sach() {
	}

	// Phuong thuc setter va getter
	public String getMaSach() {
		return maSach;
	}

	public void setMaSach(String maSach) {
		this.maSach = maSach;
	}

	public String getTenSach() {
		return tenSach;
	}

	public void setTenSach(String tenSach) {
		this.tenSach = tenSach;
	}

	public Date getNgayNhap() {
		return ngayNhap;
	}

	public void setNgayNhap(Date ngayNhap) {
		this.ngayNhap = ngayNhap;
	}

	public String getNxb() {
		return nxb;
	}

	public void setNxb(String nxb) {
		this.nxb = nxb;
	}

	public int getDonGia() {
		return donGia;
	}

	public void setDonGia(int donGia) {
		this.donGia = donGia;
	}

	public int getSoLuong() {
		return soLuong;
	}

	public void setSoLuong(int soLuong) {
		this.soLuong = soLuong;
	}
	
	// Phương thức nghiệp vụ
	
	
}
