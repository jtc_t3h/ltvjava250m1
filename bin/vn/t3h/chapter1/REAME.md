---- Chapter 1: Tổng quan -----------
1. Khái niệm
- program, languge, coding
- Bytecode: ngôn ngữ trung gian -> Chương trình Java chạy độc lập platform (OS)
- Phân loại ứng dung:
  + Java SE -> ứng dụng Desktop (Window form) -> học 2 module 1, 2
  + Java EE -> ưng dụng Web (client - server) -> học module 3, 4
  + Java ME -> ứng dụng Mobile app -> lỗi thời -> được thay thế dùng Android

2. Cài đặt môi trường
- Bộ Java SDK
  + JRE - Java Runtime Environment
  + JDK - Java Development Kit (SDK) -> Cài đặt bộ này

  -> Cách kiểm máy có cài đặt java hay chưa? -> command line (terminal) -> gõ lệnh> java -version
- Môi trường soạn thảo
  + Thô sơ: notepad[++], sublime text, editplus+
  + IDE - intergrated development envirnment -> tích API: Eclipse IDE (cài đặt), Netbeans IDE, Intellij IDE, ...

3. Xây dựng ưng dụng Java: -> Chỉ có 1 lớp hiện thực 1 tác vụ (Hellowworld xuất lời chào)
- B1: Tạo lớp trong Java -> HelloWorld
- B2: thực hiện chạy chương trình -> 2 bước:
  + Compile: > javac file.java  (javac HelloWorld.java) -> chuyển Java Code - Bytecode (sinh ra file có HelloWorld.class)
  + runtime: > java className   (java HelloWorld)       -> hiển thị kết quả
 
4. Giới thiệu Eclipse:
- Tạo ra project -> có nhiều loại project khác nhau (cấu trúc) -> chọn Java Project 
- Tạo class

* Đối với các bài tập dùng thư SWING -> Winform -> hỗ trợ kéo thả giao diện (GUI - Graphic User Interface)
-> Cài đặt plugin: Window Builder Pro 
   + Vào Help -> Eclipse marketplace -> search Window builder -> chọn phiên bản Window builder để install
-> Giới thiệu GUI

5. Tạo ứng dụng hiện thị lời chào
+ Thiết lập layout
+ Kéo thả component theo mục đích của bài tập
  - thuộc tính
    + variable
    + text
  - Xử lý sự kiện
    + double-click vào button


