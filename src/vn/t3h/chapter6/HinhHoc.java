package vn.t3h.chapter6;

public class HinhHoc {
	
	
	public HinhHoc() {
		System.out.println("HinhHoc constructor with no args.");
	}
	
	public HinhHoc(String name) {
		System.out.println("HinhHoc constructor with 1 args is " + name);
	}

	public void displayInfo() {
		System.out.println("Hinh Hoc");
	}
	
	public double tinhCV() {
		System.out.println("HinhHoc tinh cv.");
		return 0.0;
	}
}
