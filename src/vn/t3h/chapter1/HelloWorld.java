package vn.t3h.chapter1;

import javax.swing.JOptionPane;

public class HelloWorld {

	/**
	 * Hàm entry point - điểm bắt đầu thực thi của ứng dụng
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("Hello World");
		JOptionPane.showMessageDialog(null, "Hello World.");
	}

}
