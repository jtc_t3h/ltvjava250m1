package vn.t3h.chapter9;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class ReadFile {

	public static void main(String[] args) throws IOException {

		FileInputStream in = null;
		try {
			in = new FileInputStream("src/vn/t3h/chapter9/in.txt");
			
			int i;
			while ( (i = in.read()) != -1) {
				System.out.println((char)i);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (in != null) {
				in.close();
			}
		}
		
		
		
		
	}

}
