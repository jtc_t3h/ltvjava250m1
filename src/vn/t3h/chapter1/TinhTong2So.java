package vn.t3h.chapter1;

import java.util.Scanner;

public class TinhTong2So {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
				
		// Nhap so hang 1
		System.out.print("Nhap a: ");
		int a = sc.nextInt();
		
		// Nhap so hang 2
		System.out.print("Nhap b: ");
		int b = sc.nextInt();
		
		// Tinh tong
		int tong = a + b;
		System.out.println("Tong = " + tong);
	}
}

