package vn.t3h.chapter6;

public interface ShapInterface {

	public double PI = 3.14D;
	
	public double perimeter();
	public double area();
}
