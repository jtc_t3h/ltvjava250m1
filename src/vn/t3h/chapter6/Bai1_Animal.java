package vn.t3h.chapter6;

public class Bai1_Animal {

	private String name;
	private String image;
	
	public Bai1_Animal() {
	}

	public Bai1_Animal(String name, String image) {
		this.name = name;
		this.image = image;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}
	
	public String sayHello() {
		return "Hello";
	}
}
