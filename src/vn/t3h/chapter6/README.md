------ Đặc tính của lập trình HDT ---------
1. Tính thừa kế
- Thuật ngữ: general class - specific class, base class - derive class, supper class - sub class
- Đơn thừa kế
- Hiện thực thừa kế dùng từ khóa extends
2. Tính đa hình
- Định nghĩa: Hành vi của lớp cha được thay thế bởi hành vi của lớp con.

- Khái niệm:
  + Ghi đè hàm - overidding: có quan hệ cha con
    - Signiture (đặc tả hàm) của lớp con phải tương ứng với hàm được khái ở lớp cha
    - Không làm giảm mức độ đặc tả truy cập
  + Nạp chồng hàm - overloading: Trên 1 lớp có nhiều hàm cùng tên, nhưng khác:
     - Kiểu dữ liệu trả về
     - Đối số:
         + Kiểu dữ liệu của đối số
         + Số lượng đối số
     - Hàm khởi tạo -> nạp chồng hàm
     
- Từ khóa super -> gọi thành phần của lớp cha:
  + Hàm khởi tạo -> super([parameter])
  + Hàm xử lý nghiệp vụ -> super.methodName([parameter])
  
- This: đại diện cho đối tượng chính lớp đó, dùng để phân biệt biến cục bộ vs biến thực thể
  + Hàm khởi tạo
  + Hàm setter

- Từ khóa static:
  + Sử dụng: 3 + (1 - dùng với class)
    - static variable: biến tĩnh
    - static method: hàm tĩnh (vd. hàm main())
    - static block: khối lệnh tĩnh -> static {}
    * static nested class -> khai báo 1 lớp static thuộc 1 lớp khác
    
  + Ý nghĩa dùng static:
    - Biên dịch trước
    - Chia sẻ dữ liệu
  + Gọi thành phần static:
    - Gọi thông qua tên Lớp: ClassName.static
    - Gọi thông qua đối tượng: varName.static
    
3. Tính trừu tượng -> abstract
- Ý nghĩa:
  + Thể hiện cho đối tượng không có thực
  + Tối ưu code
- Sử dụng cho 2 trường hợp:
  + abstract method -> bắt buộc lớp phải abstract
  + abstract class
* Không tạo đối tượng cho lớp abstract (không dùng toán tử new)

4. Tính bao đóng - encapsulation -> giao diện - interface
- Tương tự ngôn C -> file .h 
- Dùng để khai báo hàm (Không có hiện thực) -> cũng có thể coi là abstract class
- Đa thừa kế
  
  
  
  
  
  
