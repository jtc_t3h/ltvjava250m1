package vn.t3h.chapter7;

public class Bai4_Cat {

	private String name;

	public Bai4_Cat() {}
	
	public Bai4_Cat(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
}
