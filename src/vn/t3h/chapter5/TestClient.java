package vn.t3h.chapter5;

public class TestClient {

	public static void main(String[] args) {

		PhanSo ps = null; // object
		ps = new PhanSo(); // instance
		
//		PhanSo ps1 = new PhanSo(1,2);
//		PhanSo ps2 = new PhanSo(3,4);
//		
		PhanSo kq = null;
//		System.out.println(kq.getTuSo() + "/" + kq.getMauSo());
		
		int par = 1;
		changeValue(par);
		System.out.println(par); // Kết quả của biến par: 1 -> truyền tham trị | 11 -> truyền tham biến
		
		System.out.println("kq = " + kq); // null
		addPhanSo(kq);
		System.out.println("kq = " + kq.getTuSo()); // null -> truyền tham trị | khác null -> truyền tham biến
		
		int[] arr = {1};
		changeValue(arr);
		System.out.println(arr[0]); // 1 | 11
		
		////
		varLengthMethod();
		varLengthMethod(1);
		varLengthMethod(1,2,3,4,5);
	}
	
	
	public static void changeValue(int par) {
		par = par + 10;
	}
	
	public static void changeValue(int[] arr) {
		arr[0] += 10; 
	}

	public static void addPhanSo(PhanSo kq) {
		PhanSo ps1 = new PhanSo(1,2);
		PhanSo ps2 = new PhanSo(3,4);
		
		PhanSo temp = ps1.operatorAdd(ps2);
		kq = temp;
	}
	
	public static void varLengthMethod(int ...var) {
		// duyệt danh sách các đối số truyền -> duyệt tương tủ
		for(int element: var) {
			System.out.println(element);
		}
	}
}
