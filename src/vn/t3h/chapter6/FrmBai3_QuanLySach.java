package vn.t3h.chapter6;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTabbedPane;
import javax.swing.JScrollPane;
import javax.swing.JList;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class FrmBai3_QuanLySach extends JFrame {

	private JPanel contentPane;
	private JTextField txtMaSach;
	private JTextField txtNgayNhap;
	private JTextField txtDonGia;
	private JTextField txtTenSach;
	private JTextField txtNXB;
	private JTextField txtSoLuong;
	private ButtonGroup bgSach = new ButtonGroup();
	private JTextField txtThue;
	private JTextField txtTongTien1;
	private JTextField txtTongTien2;
	private JTabbedPane tabbedPane;
	private JPanel panel_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai3_QuanLySach frame = new FrmBai3_QuanLySach();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai3_QuanLySach() {
		setTitle("Quản lý sách");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 833, 632);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "Th\u00F4ng tin chung", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.setBounds(6, 6, 816, 137);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Mã sách");
		lblNewLabel.setBounds(17, 27, 115, 16);
		panel.add(lblNewLabel);
		
		txtMaSach = new JTextField();
		txtMaSach.setBounds(136, 22, 165, 26);
		panel.add(txtMaSach);
		txtMaSach.setColumns(10);
		
		JLabel label = new JLabel("New label");
		label.setBounds(17, 60, 115, 16);
		panel.add(label);
		
		txtNgayNhap = new JTextField();
		txtNgayNhap.setColumns(10);
		txtNgayNhap.setBounds(136, 55, 165, 26);
		panel.add(txtNgayNhap);
		
		JLabel label_1 = new JLabel("New label");
		label_1.setBounds(17, 94, 115, 16);
		panel.add(label_1);
		
		txtDonGia = new JTextField();
		txtDonGia.setColumns(10);
		txtDonGia.setBounds(136, 89, 165, 26);
		panel.add(txtDonGia);
		
		JLabel label_2 = new JLabel("New label");
		label_2.setBounds(417, 27, 115, 16);
		panel.add(label_2);
		
		txtTenSach = new JTextField();
		txtTenSach.setColumns(10);
		txtTenSach.setBounds(536, 22, 165, 26);
		panel.add(txtTenSach);
		
		JLabel label_3 = new JLabel("New label");
		label_3.setBounds(417, 60, 115, 16);
		panel.add(label_3);
		
		txtNXB = new JTextField();
		txtNXB.setColumns(10);
		txtNXB.setBounds(536, 55, 165, 26);
		panel.add(txtNXB);
		
		JLabel label_4 = new JLabel("New label");
		label_4.setBounds(417, 94, 115, 16);
		panel.add(label_4);
		
		txtSoLuong = new JTextField();
		txtSoLuong.setColumns(10);
		txtSoLuong.setBounds(536, 89, 165, 26);
		panel.add(txtSoLuong);
		
		JRadioButton rdbSGK = new JRadioButton("Sách giáo khoa");
		rdbSGK.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				tabbedPane.setSelectedIndex(0);
				tabbedPane.setSelectedComponent(panel_1);
			}
		});
		rdbSGK.setSelected(true);
		rdbSGK.setBounds(243, 168, 141, 23);
		contentPane.add(rdbSGK);
		bgSach.add(rdbSGK);
		
		JRadioButton rdbSTK = new JRadioButton("Sách tham khảo");
		rdbSTK.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				// Active tab : Sach tham khao -> code here ???
				tabbedPane.setSelectedIndex(1);
			}
		});
		rdbSTK.setBounds(445, 168, 141, 23);
		contentPane.add(rdbSTK);
		bgSach.add(rdbSTK);
		
		JLabel label_5 = new JLabel("New label");
		label_5.setBounds(17, 208, 115, 16);
		contentPane.add(label_5);
		
		JLabel label_6 = new JLabel("New label");
		label_6.setBounds(417, 208, 115, 16);
		contentPane.add(label_6);
		
		txtThue = new JTextField();
		txtThue.setColumns(10);
		txtThue.setBounds(536, 203, 165, 26);
		contentPane.add(txtThue);
		
		JComboBox cbbTinhTrang = new JComboBox();
		cbbTinhTrang.setModel(new DefaultComboBoxModel(new String[] {"", "Mới", "Cũ"}));
		cbbTinhTrang.setBounds(144, 204, 158, 27);
		contentPane.add(cbbTinhTrang);
		
		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(6, 277, 821, 327);
		contentPane.add(tabbedPane);
		
		panel_1 = new JPanel();
		tabbedPane.addTab("Sách giáo khoa", null, panel_1, null);
		panel_1.setLayout(null);
		
		JLabel label_7 = new JLabel("New label");
		label_7.setBounds(25, 24, 115, 16);
		panel_1.add(label_7);
		
		txtTongTien1 = new JTextField();
		txtTongTien1.setColumns(10);
		txtTongTien1.setBounds(144, 19, 299, 26);
		panel_1.add(txtTongTien1);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(6, 58, 788, 217);
		panel_1.add(scrollPane);
		
		JList lst1 = new JList();
		scrollPane.setViewportView(lst1);
		
		JPanel panel_2 = new JPanel();
		tabbedPane.addTab("Sách tham khảo", null, panel_2, null);
		panel_2.setLayout(null);
		
		JLabel label_8 = new JLabel("New label");
		label_8.setBounds(25, 24, 115, 16);
		panel_2.add(label_8);
		
		txtTongTien2 = new JTextField();
		txtTongTien2.setColumns(10);
		txtTongTien2.setBounds(144, 19, 299, 26);
		panel_2.add(txtTongTien2);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(6, 58, 788, 250);
		panel_2.add(scrollPane_1);
		
		JList lst2 = new JList();
		scrollPane_1.setViewportView(lst2);
		
		JButton btnNhapSach = new JButton("Nhập Sách");
		btnNhapSach.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Lấy thông từ input -> Tạo ra đối sách tương ứng
				Bai3_Sach sach = null;
				if(rdbSGK.isSelected()) {  // Sách giáo khoa
					sach = new Bai3_SachGiaoKhoa();
					((Bai3_SachGiaoKhoa)sach).setTinhTrang(cbbTinhTrang.getSelectedIndex() == 1? true: false);
				} else { // Sách tham khảo
					sach = new Bai3_SachThamKhao();
					((Bai3_SachThamKhao)sach).setThue(Integer.parseInt(txtThue.getText().trim()));
				}
				sach.setMaSach(txtMaSach.getText().trim());
				sach.setTenSach(txtTenSach.getText().trim());
				
				// chuyen String -> Date
				sach.setNgayNhap(stringToDate(txtNgayNhap.getText().trim(), "dd/MM/yyyy"));  
				sach.setNxb(txtNXB.getText().trim());
				sach.setDonGia(Integer.parseInt(txtDonGia.getText().trim()));
				sach.setSoLuong(Integer.parseInt(txtSoLuong.getText().trim()));
				
				
			}
		});
		btnNhapSach.setBounds(144, 236, 117, 29);
		contentPane.add(btnNhapSach);
		
		JButton btnTiepTuc = new JButton("Tiếp tục");
		btnTiepTuc.setBounds(415, 236, 117, 29);
		contentPane.add(btnTiepTuc);
	}
	
	/**
	 * Convert string to java.util.Date
	 * @param sDate
	 * @return
	 */
	private Date stringToDate(String sDate, String pattern) {
		DateFormat formatter = new SimpleDateFormat(pattern);
		Date date = null;
		try {
			date = formatter.parse(sDate);
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return date;
	}
}
