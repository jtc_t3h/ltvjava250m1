package vn.t3h.chapter5;

import javax.activation.MailcapCommandMap;

public class PhanSo {

	private int tuSo;
	private int mauSo;
	
	public PhanSo() {
	}

	public PhanSo(int tuSo, int mauSo) {
		this.tuSo = tuSo;
		this.mauSo = mauSo;
	}

	public int getTuSo() {
		return tuSo;
	}

	public void setTuSo(int tuSo) {
		this.tuSo = tuSo;
	}

	public int getMauSo() {
		return mauSo;
	}

	public void setMauSo(int mauSo) {
		this.mauSo = mauSo;
	}
	
	// Hàm nghiệp vụ
	
	
	/**
	 * Cộng 2 phân số
	 * @param ps2
	 * @return
	 */
	public PhanSo operatorAdd(PhanSo ps2) {
		int tuSo = this.tuSo * ps2.getMauSo() + this.mauSo * ps2.getTuSo();
		int mauSo = this.mauSo * ps2.getMauSo();
		
		return new PhanSo(tuSo, mauSo);
	}
}
