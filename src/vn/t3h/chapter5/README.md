--------- Chương 5: Đối tượng ---------------
1. Thuật ngữ:
- Đối tượng (thế giới thực): object
  + Trạng thái của đối tượng
  + Hành vi của đối tượng
- Lớp - Class (mô phỏng cho đối tượng ở thế giới thực)
  + Thuộc tính -> trạng thái của đối tượng: biến thực thể - instance variable
  + Phương thức - hàm -> hành vi của đối tượng: Phân loại hàm
    - Hàm khởi tạo
      + Mặc định: không có tham số
        - Ẩn -> chỉ tồn tại khi đối tượng không có khai báo hàm khởi tạo.
        - Tường minh
      + Có tham số
    - Hàm getter và setter
    - Hàm nghiệp vụ
- instance - thực thể
    
2. Khai báo lớp -> cú pháp:
   [Modifier] class ClassName{
     // code here
   }
3. Khai báo hàm
- Cú pháp:
   [Modifier] DataType functionName([parameters]){
     // code here
   }
   Trong đó:
   + DataType: void -> Không có trả về kết quả
   + DataType: int, double, ... -> phải có kết quả tương ứng trả về

- Phương thức có đối số thay đổi (không xác định) - Variable-length argument method: có [0-N] 
  [modifier] DataType methodName([par1, par2,...,] DataType ... var) {}


- Gọi hàm:
  + khái niệm: 
    - đối số hình thức: biến được định nghĩa trong hàm
    - đối số thực: biến được truyền vào khi gọi hàm -> đã có giá trị
  + Truyền đối số thực:
    - Truyền tham trị - by pass value: kiểu dữ liệu của đối số là kiểu: cơ bản, kiểu wrapper class
    - Truyền tham biến - by pass reference: kiểu dữ liệu của đối số là reference: mảng, class, interface, enum
    
    
     