package vn.t3h.chapter6;

public class HinhTron extends HinhHoc implements ShapInterface {

	private double r;
	
	public HinhTron() {
		super("Hinh Tron");
	}
	
	@Override
	public void displayInfo() {
		System.out.println("Hinh tron");
	}
	
	public double tinhCV() {
		super.tinhCV();
		return Math.PI * 2 * r;
	}

	@Override
	public double perimeter() {
		return 2 * r * PI;
	}

	@Override
	public double area() {
		return r * r * PI;
	}
}
