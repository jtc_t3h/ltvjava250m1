package vn.t3h.chapter6;

public class Bai3_SachGiaoKhoa extends Bai3_Sach {

	// Thuộc tính riêng
	private boolean tinhTrang;
	
	public Bai3_SachGiaoKhoa() {
	}

	public boolean isTinhTrang() {
		return tinhTrang;
	}

	public void setTinhTrang(boolean tinhTrang) {
		this.tinhTrang = tinhTrang;
	}
	
	
}
