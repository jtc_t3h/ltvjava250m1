package vn.t3h.chapter2;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class FrmBai2_TimKiemChuoi extends JFrame {

	private JPanel contentPane;
	private JTextField txtS1;
	private JTextField txtS2;
	private JTextField txtKQ;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai2_TimKiemChuoi frame = new FrmBai2_TimKiemChuoi();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai2_TimKiemChuoi() {
		setTitle("Tìm kiếm chuỗi");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 587, 195);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Nhập chuỗi 1:");
		lblNewLabel.setBounds(17, 18, 146, 16);
		contentPane.add(lblNewLabel);
		
		txtS1 = new JTextField();
		txtS1.setBounds(175, 13, 394, 26);
		contentPane.add(txtS1);
		txtS1.setColumns(10);
		
		txtS2 = new JTextField();
		txtS2.setColumns(10);
		txtS2.setBounds(175, 46, 394, 26);
		contentPane.add(txtS2);
		
		JLabel lblNhpChui = new JLabel("Nhập chuỗi 2:");
		lblNhpChui.setBounds(17, 51, 146, 16);
		contentPane.add(lblNhpChui);
		
		txtKQ = new JTextField();
		txtKQ.setEditable(false);
		txtKQ.setColumns(10);
		txtKQ.setBounds(175, 84, 394, 26);
		contentPane.add(txtKQ);
		
		JLabel lblKtQu = new JLabel("Kết quả:");
		lblKtQu.setBounds(17, 89, 146, 16);
		contentPane.add(lblKtQu);
		
		JButton btnNewButton = new JButton("Tìm kiếm");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Tìm kiếm chuỗi s2 có tồn tại trong chuỗi s1 -> s2 thuộc về s1 hoặc s2 không thuộc s1
				String s1 = txtS1.getText().trim();
				String s2 = txtS2.getText().trim();
				
				if (s1.toLowerCase().contains(s2.toLowerCase())) {
					txtKQ.setText("s2 thuộc về s1");
				} else {
					txtKQ.setText("s2 không thuộc s1");
				}
			}
		});
		btnNewButton.setBounds(175, 122, 117, 29);
		contentPane.add(btnNewButton);
	}
}
