package vn.t3h.chapter1;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class FrmHello extends JFrame {

	private JPanel contentPane;
	private JTextField txtHoTen;
	private JLabel lblHienThi;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmHello frame = new FrmHello();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmHello() {
		setTitle("Hello world");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 171);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblHoTen = new JLabel("Họ tên");
		lblHoTen.setBounds(6, 20, 61, 16);
		contentPane.add(lblHoTen);
		
		txtHoTen = new JTextField();
		txtHoTen.setBounds(76, 15, 345, 26);
		contentPane.add(txtHoTen);
		txtHoTen.setColumns(10);
		
		JButton btnHinThLi = new JButton("Hiển thị lời chào");
		btnHinThLi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// B1. Lấy thông tin -> ho ten người dùng nhập vào -> sử dụng biến (variable) và gọi thuộc tính tương ứng
				String hoTen = txtHoTen.getText();
				
				// B2. Hiện thị lời chào
				lblHienThi.setText("Xin chào " + hoTen);
			}
		});
		btnHinThLi.setBounds(156, 60, 149, 29);
		contentPane.add(btnHinThLi);
		
		lblHienThi = new JLabel("");
		lblHienThi.setBounds(6, 101, 427, 16);
		contentPane.add(lblHienThi);
	}
}
