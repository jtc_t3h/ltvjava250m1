package vn.t3h.chapter6;

public abstract class GiaoVien {

	private String name;
	private int age;
	
	public abstract double tinhLuong();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}
	
	
}
