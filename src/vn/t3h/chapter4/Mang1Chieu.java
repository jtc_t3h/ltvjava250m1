package vn.t3h.chapter4;

import java.util.stream.Stream;

public class Mang1Chieu {

	public static void main(String[] args) {

		int[] arr1 = new int[5];
		int arr2[] = { 1, 2, 3, 4, 5 };
		Integer arr3[] = new Integer[] { 1, 2, 3, 4, 5 };
		System.out.println(arr1.length); // Lay kich thuoc mang
		System.out.println(arr2[3]); // Lay phan tu co index = 3 -> phan tu thu 4

		// duyet cac phan tu trong mang
		for (int element : arr1) {
			System.out.println(element);
		}

		for (int idx = 0; idx <= arr2.length; idx++) {
			System.out.println(arr2[idx]);
		}

		Stream.of(arr3).forEach(element -> System.out.println(element));
	}

}
