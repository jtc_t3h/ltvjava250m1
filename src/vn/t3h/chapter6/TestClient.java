package vn.t3h.chapter6;

public class TestClient {
	
	static {
		System.out.println("Static block.");
	}
	
	static class StaticClass{
		
	}
	
	public static void main(String[] args) {
		HinhHoc hh = new HinhHoc();  // t0: hh: Hinh Hoc
		hh.displayInfo();
		
		hh = new HinhTron();  // t1: hh -> Hinh Tron
		hh.displayInfo();
		
		hh = new HinhChuNhat();
		hh.displayInfo(); 
		
		hh = new HinhTamGiac();
		hh.displayInfo();
		
		People p1 = new People();
		People p2 = new People();
		
		People.total = 10;
		System.out.println(p2.total);
		
		
	}
}
