package vn.t3h.chapter7;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class FrmBai4_QuanLyCuaHang extends JFrame {

	private List<PetManager<Bai4_Dog>> listOfDog = new ArrayList<PetManager<Bai4_Dog>>();
	private List<PetManager<Bai4_Cat>> listOfCat = new ArrayList<PetManager<Bai4_Cat>>();
	
	private JPanel contentPane;
	private JTextField txtDog;
	private JTextField txtCat;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai4_QuanLyCuaHang frame = new FrmBai4_QuanLyCuaHang();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai4_QuanLyCuaHang() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 659, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnThmMi = new JButton("Thêm mới");
		btnThmMi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Lấy danh sách Dog, Cat từ người dùng -> add vào listOfDog, listOfCat
				String sCats = txtCat.getText();
				String[] catNames = sCats.split(",");
				for(String catName: catNames) {
					Bai4_Cat cat = new Bai4_Cat(catName);
					PetManager<Bai4_Cat> pCat = new PetManager<Bai4_Cat>();
					pCat.setPet(cat);
					
					listOfCat.add(pCat);
				}
				
				// Hiện thỉ 2 danh sách ra JList
				
			}
		});
		btnThmMi.setBounds(58, 138, 117, 29);
		contentPane.add(btnThmMi);
		
		txtDog = new JTextField();
		txtDog.setBounds(150, 22, 477, 26);
		contentPane.add(txtDog);
		txtDog.setColumns(10);
		
		txtCat = new JTextField();
		txtCat.setColumns(10);
		txtCat.setBounds(150, 60, 477, 26);
		contentPane.add(txtCat);
	}
}
