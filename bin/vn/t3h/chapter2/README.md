------------------------ Chapter 2 ----------------------------------------------------
1. Nội dung
- Data type - kiễu dữ liệu
- Khai báo biến
- khai báo hằng số
- Kiểu dữ liệu kí tự
- Kiểu dữ liệu chuỗi

2. Data type
- Đối với số nguyên -> miền giá trị -> cách tính giá trị lớn - nhỏ nhất
- Hệ số:
  + nhị phân
  + bát phân
  + thập phân
  + thập lục phân
-> chuyển đổi kiểu giữa các hệ số
- Giá trị mặc định
  + số nguyên = 0
  + số thực = 0.0
  + kiểu boolean = false
  + kiểu reference = null
-> Sự khác nhau giữa kiểu cơ bản và kiểu refernce

- Chuyển đổi kiểu dữ liệu (chỉ áp dụng cho kiểu cơ bản và loại kiểu boolean -> kiểu số)
  + tường minh: chuyển kiểu dữ liệu lớn -> nhỏ: double -> float -> long -> int -> short - char -> byte
  + ẩn: chuyển kiểu dữ liệu nhỏ -> lớn: byte -> short - char -> int -> long -> float -> double

2. Khai báo biến
- Phân loại:
  + Local variable - biến cục bộ: được sử dụng trong 1 hàm (*)
  + Instance variable - biến thực thể: là 1 thuộc tính 1 lớp đối tượng
  + Static variable - biến tĩnh: biến có đặc tả static
- Cú pháp định nghĩa:
  [Modifier] DataType varName [= value];
  
3. Khai báo hằng số
- Cú pháp: 
  [Modifier] final DataType varName = value;
  
4. Kiểu kí tự
- kiểu cơ bản - char
- Kiểu wrapper class -> Character

5. Xử lý chuỗi:
- String
  + Chỉ có kiểu Reference (ko có kiểu cơ bản):
  + khai báo biến:
    - dùng toán tử new
    - khai báo giống kiểu cơ bản
  + So sánh chuỗi:
    - dùng toán tử ==: so sánh theo đối tượng (bao gồm nhiều thuộc tính: địa chỉ ô  nhớ, thông tin khác,...)
    - dùng hàm equals(): so sánh giá trị
    - dùng compareTo: so sánh kí tự aphabet -> trả về số nguyên:
      + > 0: thì trước > sau
      + = 0: thì 2 chuỗi bằng nhau
      + < 0: thì trước < sau 
  + Vấn đề immutable -> Tạo ra nhiều ô nhớ rác -> Gabage colection (tiến trình ngầm đi dọn rác)
- StringBuffer
- StringBuilder
  + chỉ có 1 cách khai báo đối tượng -> dùng toán tử new
  + Tương tự như StringBuffer -> khác nhau ở chỗ không hỗ trợ đồng bộ (synr)
- StringTokenizer: lớp hỗ trợ tách chuỗi (có nhiều cách tách chuỗi)
- ...