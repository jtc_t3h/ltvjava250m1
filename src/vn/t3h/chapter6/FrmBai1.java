package vn.t3h.chapter6;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.Font;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.util.Locale.Category;
import java.awt.event.ActionEvent;

public class FrmBai1 extends JFrame {

	private JPanel contentPane;
	private JTextField txtName;
	private JLabel lblKQ;
	private JLabel lblImage;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai1 frame = new FrmBai1();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai1() {
		setTitle("Animal say hello");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 488, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblName = new JLabel("Name");
		lblName.setBounds(16, 17, 61, 16);
		contentPane.add(lblName);
		
		txtName = new JTextField();
		txtName.setBounds(87, 12, 143, 26);
		contentPane.add(txtName);
		txtName.setColumns(10);
		
		JLabel lblAnimal = new JLabel("Animal");
		lblAnimal.setBounds(16, 55, 61, 16);
		contentPane.add(lblAnimal);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Dog", "Cat", "Pig"}));
		comboBox.setBounds(87, 50, 143, 27);
		contentPane.add(comboBox);
		
		JButton btnSayHello = new JButton("Say Hello");
		btnSayHello.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//B1. Lây dữ liệu người dùng nhập vào và kiem tra dang chon con vat nao
				String name = txtName.getText().trim();
//				if(comboBox.getSelectedIndex() == 0) {// Dog
//					// Tạo đối Animal tương ứng
//					Bai1_Dog dog = new Bai1_Dog();
//					
//					// Hiển thị kết quả: nhãn và hình
//					lblKQ.setText(name + " " + dog.sayHello());
//					lblImage.setIcon(new ImageIcon(FrmBai1.class.getResource("/vn/t3h/resource/dog.jpg")));
//					
//				} else if ("Cat".equals(comboBox.getSelectedItem())) { // Cat
//					Bai1_Cat cat = new Bai1_Cat();
//					
//					lblKQ.setText(name + " " + cat.sayHello());
//					lblImage.setIcon(new ImageIcon(FrmBai1.class.getResource("/vn/t3h/resource/meo.png")));
//				} else {  // Pig
//					Bai1_Pig pig = new Bai1_Pig();
//					
//					lblKQ.setText(name + " " + pig.sayHello());
//					lblImage.setIcon(new ImageIcon(FrmBai1.class.getResource("/vn/t3h/resource/pig.jpg")));
//				}
				
				Bai1_Animal animal = null;
				ImageIcon icon = null;
				if(comboBox.getSelectedIndex() == 0) {// Dog
					animal = new Bai1_Dog();
					icon = new ImageIcon(FrmBai1.class.getResource("/vn/t3h/resource/dog.jpg"));
				} else if ("Cat".equals(comboBox.getSelectedItem())) { // Cat
					animal = new Bai1_Cat();
					icon = new ImageIcon(FrmBai1.class.getResource("/vn/t3h/resource/meo.png"));
				} else {  // Pig
					animal = new Bai1_Pig();
					icon = new ImageIcon(FrmBai1.class.getResource("/vn/t3h/resource/pig.jpg"));
				}
				lblKQ.setText(name + " " + animal.sayHello());
				lblImage.setIcon(icon);
			}
		});
		btnSayHello.setBounds(87, 112, 117, 29);
		contentPane.add(btnSayHello);
		
		lblKQ = new JLabel("");
		lblKQ.setFont(new Font("Lucida Grande", Font.BOLD, 15));
		lblKQ.setForeground(Color.RED);
		lblKQ.setBounds(16, 233, 453, 26);
		contentPane.add(lblKQ);
		
		lblImage = new JLabel("");
		lblImage.setIcon(new ImageIcon(FrmBai1.class.getResource("/vn/t3h/resource/dog.jpg")));
		lblImage.setBounds(256, 17, 199, 204);
		contentPane.add(lblImage);
	}
}
