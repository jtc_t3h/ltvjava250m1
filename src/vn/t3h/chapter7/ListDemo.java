package vn.t3h.chapter7;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ListDemo {

	public static void main(String[] args) {

		// Tao đối tượng List
		// Cach 1:
		List list1 = new LinkedList();
		list1.add(1);
		list1.add(3);
		list1.add("Hello");
		list1.add(1.0);
		
		
		// Cach 2:
		ArrayList<String> list2 = new ArrayList<String>();
		list2.add("Hello");
		list2.add("world");

		
		Integer arrInt[] = {1,2,3,4,5};
		Float arrFlt[] = {1.1f, 2.2f};
		String arrStr[] = {"Hello", "World"};
		
//		for(int e: arrInt) {
//			System.out.println(e);
//		}
//		
//		for(float e: arrFlt) {
//			System.out.println(e);
//		}
//		
//		for(String e: arrStr) {
//			System.out.println(e);
//		}
		
		printArray(arrInt);
		printArray(arrFlt);
		printArray(arrStr);
		
		Set set = new HashSet();
		set.add(1);
		set.add(2);
		set.add(1);
		
		Map map = new HashMap();
		map.put(123, "hello");
		map.put(123, 123);
		
		map.put(null, 123);
 	}
	
	private static <T> void printArray(T [] arr) {
		for(T e: arr) {
			System.out.println(e);
		}
	}

}
