package vn.t3h.chapter3;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class FrmDemo extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmDemo frame = new FrmDemo();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmDemo() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton button = new JButton("Open Dialog");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DlgDemo dlg = new DlgDemo();
				dlg.setVisible(true);
			}
		});
		button.setBounds(159, 6, 117, 29);
		contentPane.add(button);
		
		JPanel panel = new JPanel();
		panel.setBounds(6, 48, 438, 224);
		contentPane.add(panel);
		panel.setLayout(null);
		
		// add pnDemo
		PnDemo pnDemo = new PnDemo();
		pnDemo.setBounds(6, 6, 426, 212);
		panel.add(pnDemo);
		
	}

}
