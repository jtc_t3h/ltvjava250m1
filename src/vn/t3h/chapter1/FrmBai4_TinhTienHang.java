package vn.t3h.chapter1;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class FrmBai4_TinhTienHang extends JFrame {

	private JPanel contentPane;
	private JTextField txtSoLuong;
	private JTextField txtDonGia;
	private JTextField txtThanhTien;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai4_TinhTienHang frame = new FrmBai4_TinhTienHang();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai4_TinhTienHang() {
		setTitle("Tính giá trị hóa đơn");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 346, 191);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Số lượng:");
		lblNewLabel.setBounds(22, 20, 89, 16);
		contentPane.add(lblNewLabel);
		
		txtSoLuong = new JTextField();
		txtSoLuong.setBounds(125, 15, 151, 26);
		contentPane.add(txtSoLuong);
		txtSoLuong.setColumns(10);
		
		JLabel lblnGi = new JLabel("Đơn giá:");
		lblnGi.setBounds(22, 53, 89, 16);
		contentPane.add(lblnGi);
		
		txtDonGia = new JTextField();
		txtDonGia.setColumns(10);
		txtDonGia.setBounds(125, 48, 151, 26);
		contentPane.add(txtDonGia);
		
		JLabel lblThnhTin = new JLabel("Thành tiền:");
		lblThnhTin.setBounds(22, 86, 89, 16);
		contentPane.add(lblThnhTin);
		
		txtThanhTien = new JTextField();
		txtThanhTien.setEditable(false);
		txtThanhTien.setColumns(10);
		txtThanhTien.setBounds(125, 81, 151, 26);
		contentPane.add(txtThanhTien);
		
		JLabel lblVnd = new JLabel("VND");
		lblVnd.setBounds(287, 53, 61, 16);
		contentPane.add(lblVnd);
		
		JLabel label_2 = new JLabel("VND");
		label_2.setBounds(287, 86, 61, 16);
		contentPane.add(label_2);
		
		JButton btnTinhTien = new JButton("Tính tiền");
		btnTinhTien.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// code here
				// B1. Lấy thông tin người dùng nhập -> số lượng và đơn giá
				String sSoLuong = txtSoLuong.getText().trim();  // hàm trim() -> xóa bỏ khoảng trắng đầu và cuối chuỗi
				String sDonGia = txtDonGia.getText().trim();
				
				// B2. chuyển dữ liệu từ chuỗi -> số thực -> dung hàm parseType (eg. Integer.parseInt, Double.parseDouble)
				// Có thể sử dụng hàm valueOf() (eg. Integer.valueOf, Double.valueOf)
				double dSoLuong = Double.parseDouble(sSoLuong);
				double dDonGia = Double.valueOf(sDonGia);
				
				// B3. Thực tính tiền và hiển thị kết quả --> chuyển số -> chuỗi (chỉ có hàm valueOf)
				double dTinhTien = dSoLuong * dDonGia;
				txtThanhTien.setText(String.valueOf(dTinhTien));
			}
		});
		btnTinhTien.setBounds(105, 114, 117, 34);
		contentPane.add(btnTinhTien);
	}
}
